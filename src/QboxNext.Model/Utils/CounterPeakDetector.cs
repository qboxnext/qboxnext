﻿using System;
using QboxNext.Model.Qboxes;
using QboxNext.Storage;

namespace QboxNext.Model.Utils
{
    /// <summary>
    /// Class to detect peaks in the counter values.
    /// </summary>
    public class CounterPeakDetector : ICounterPeakDetector
    {
        private readonly decimal _maxPeakValue;

        public CounterPeakDetector(decimal maxPeakValue)
        {
            _maxPeakValue = maxPeakValue;
        }

        public bool IsPeak(DateTime measurementTime, ulong value, decimal formula, Record lastValue, Counter counter)
        {
            if (lastValue != null && lastValue.Time < measurementTime)
            {
                // Gas is measured every hour (* 60) (and saved every minute)
                if (counter.IsGasCounter)
                {
                    formula = formula * 60;
                }

                decimal delta = value < lastValue.Raw ? 0m : (value - lastValue.Raw) / (formula == 0 ? 1 : formula);

                int minutes = (int)(measurementTime - lastValue.Time).TotalMinutes;
                minutes = minutes <= 0 ? 1 : minutes;

                decimal factor = Convert.ToDecimal(minutes < 60d ? 60.0 / minutes : 1.0);
                decimal consumption = delta * factor * 1000m;

                return consumption > _maxPeakValue;
            }

            return false;
        }
    }
}