﻿using System;
using QboxNext.Model.Qboxes;
using QboxNext.Storage;

namespace QboxNext.Model.Utils
{
    public interface ICounterPeakDetector
    {
        bool IsPeak(DateTime measurementTime, ulong value, decimal formula, Record lastValue, Counter counter);
    }
}
