﻿using System;
using System.Collections.Generic;
using QboxNext.Storage;

namespace QboxNext.Model
{
    /// <summary>
    /// Simple mock storage provider to always fill the slots with 42.
    /// </summary>
    public class MockStorageProvider : IStorageProvider
    {
        public MockStorageProvider(string serialNumber, int counterId, Precision precision)
        {
        }

        public void Dispose()
        {
        }

        public decimal Sum(DateTime begin, DateTime end, Unit eenheid)
        {
            throw new NotImplementedException();
        }

        public bool GetRecords(DateTime inBegin, DateTime inEnd, Unit inUnit, IList<SeriesValue> ioSlots, bool inNegate)
        {
            BuildData(ioSlots, inNegate);
            return true;
        }

        public Record GetValue(DateTime measureTime)
        {
            throw new NotImplementedException();
        }

        public Record SetValue(DateTime inMeasureTime, ulong inPulseValue, decimal inPulsesPerUnit, decimal inEurocentsPerUnit, Record inRunningTotal = null)
        {
            throw new NotImplementedException();
        }

        public void ReinitializeSlots(DateTime inFrom)
        {
            throw new NotImplementedException();
        }

        public Record FindPrevious(DateTime inMeasurementTime)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Generate some values for a series.
        /// </summary>
        private static void BuildData(IList<SeriesValue> values, bool negate)
        {
            var random = new Random();

            foreach (var value in values)
            {
                value.Value = 42;
                if (negate)
                {
                    value.Value = -value.Value;
                }
            }
        }
    }
}