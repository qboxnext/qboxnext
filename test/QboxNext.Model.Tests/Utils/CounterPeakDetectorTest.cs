﻿using System;
using NUnit.Framework;
using QboxNext.Model.Qboxes;
using QboxNext.Storage;

namespace QboxNext.Model.Utils
{
    [TestFixture]
    public class CounterPeakDetectorTest
    {
        [Test]
        public void IsPeakWithNoLastValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1), 0, 1000, null, new Counter()), Is.False);
        }


        [Test]
        public void IsPeakWithLowValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 1, 1000, lastValue, new Counter()), Is.False);
        }


        [Test]
        public void IsPeakWithHighValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 20000, 1000, lastValue, new Counter()), Is.True);
        }


        [Test]
        public void IsPeakWithHighValueWithTimestampBeforeLastValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1, 0, 1, 0)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1), 20000, 1000, lastValue, new Counter()), Is.False);
        }


        [Test]
        public void IsPeakWithLowGasValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            var counterPoco = new Counter
            {
                CounterId = 2421
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 20000, 1000, lastValue, counterPoco), Is.False);
        }


        [Test]
        public void IsPeakWithHighGasValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            var counterPoco = new Counter
            {
                CounterId = 2421
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 200000, 1000, lastValue, counterPoco), Is.True);
        }


        [Test]
        public void IsPeakWithValueLowerThanLastValueTest()
        {
            var detector = new CounterPeakDetector(20000);
            var lastValue = new Record(1, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 0, 1000, lastValue, new Counter()), Is.False);
        }


        [Test]
        public void IsPeakWithJustTooHighValueWithLastValueOneMinuteAgoTest()
        {
            // 1 Wh per minute = 60 Wh per hour
            var detector = new CounterPeakDetector(59m);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 1, 1000, lastValue, new Counter()), Is.True);
        }


        [Test]
        public void IsPeakWithJustTooLowValueWithLastValueOneMinuteAgoTest()
        {
            // 1 Wh per minute = 60 Wh per hour
            var detector = new CounterPeakDetector(60m);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 1, 0), 1, 1000, lastValue, new Counter()), Is.False);
        }


        [Test]
        public void IsPeakWithJustTooHighValueWithLastValueHalfAnHourAgoTest()
        {
            // 1 Wh per half hour = 2 Wh per hour
            var detector = new CounterPeakDetector(1m);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 30, 0), 1, 1000, lastValue, new Counter()), Is.True);
        }


        [Test]
        public void IsPeakWithJustTooLowValueWithLastValueHalfAnHourAgoTest()
        {
            // 1 Wh per half hour = 2 Wh per hour
            var detector = new CounterPeakDetector(60m);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 0, 30, 0), 1, 1000, lastValue, new Counter()), Is.False);
        }


        [Test]
        public void IsPeakWithJustTooHighValueWithLastValueTwoHoursAgoTest()
        {
            // It does not matter if the difference is one or two hours, should simply check with the actual consumption.
            var detector = new CounterPeakDetector(0.99m);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 2, 0, 0), 1, 1000, lastValue, new Counter()), Is.True);
        }


        [Test]
        public void IsPeakWithJustTooLowValueWithLastValueTwoHoursAgoTest()
        {
            // It does not matter if the difference is one or two hours, should simply check with the actual consumption.
            var detector = new CounterPeakDetector(1.0m);
            var lastValue = new Record(0, 0, 0, 0)
            {
                Time = new DateTime(2016, 1, 1)
            };
            Assert.That(detector.IsPeak(new DateTime(2016, 1, 1, 2, 0, 0), 1, 1000, lastValue, new Counter()), Is.False);
        }
    }
}
