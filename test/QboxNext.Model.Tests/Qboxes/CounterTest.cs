﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using QboxNext.Core.Utils;
using QboxNext.Logging;
using QboxNext.Model.Utils;
using QboxNext.Qserver.Core.Model;
using QboxNext.Storage;

namespace QboxNext.Model.Qboxes
{
    /// <summary>
    ///This is a test class for CounterTest and is intended
    ///to contain all CounterTest Unit Tests
    ///</summary>
    [TestFixture]
    public class CounterTest
    {
        private Mock<ILogger> _loggerMock;
        private Mock<ILoggerFactory> _loggerFactoryMock;

        [SetUp]
        public void SetUp()
        {
            _loggerMock = new Mock<ILogger>();

            _loggerFactoryMock = new Mock<ILoggerFactory>();
            _loggerFactoryMock.Setup(lf => lf.CreateLogger(It.IsAny<string>())).Returns(_loggerMock.Object);

            QboxNextLogProvider.LoggerFactory = _loggerFactoryMock.Object;
        }

        /// <summary>
        /// A test for GetSeries
        /// </summary>
        [Test]
        public void GetSeriesCounter181ShouldReturnHourlyValuesTest()
        {
            // arrange
            var target = new Counter
            {
                CounterId = 181
            };
            var from = new DateTime(2019, 1, 1);
            var to = new DateTime(2019, 1, 2);
            var model = new Mini
            {
                SerialNumber = "serial"
            };

            model.Counters.Add(target);
            CreateStorageMock(model);

            const SeriesResolution resolution = SeriesResolution.Hour;

            // act
            var actual = target.GetSeries(Unit.Raw, from, to, resolution, false);

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(24, actual.Count);
            Assert.IsNotNull(actual.Any(a => a.Value != null));
        }

        /// <summary>
        /// A test for GetSeries
        /// </summary>
        [Test]
        public void GetSeriesCounter182ShouldReturnHourlyValuesOf10Test()
        {
            // arrange
            var target = new Counter
            {
                CounterId = 182
            };
            var from = new DateTime(2019, 1, 1);
            var to = new DateTime(2019, 1, 2);
            var model = new Mini
            {
                SerialNumber = "serial"
            };

            model.Counters.Add(target);
            CreateStorageMock(model);

            const SeriesResolution resolution = SeriesResolution.Hour;

            // act
            var actual = target.GetSeries(Unit.Raw, from, to, resolution, true);

            // Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(24, actual.Count);
            Assert.AreEqual(-42m, actual[0].Value);
        }

        [Test]
        public void WhenCounterGetSeriesIsCalledTheStorageProviderShouldBeRetrievedTest()
        {
            // arrange
            var qbox = new Qbox
            {
                SerialNumber = "Dummy-Test1"
            };
            var counter = new Counter { Qbox = qbox, CounterId = 1 };
            var moq = new Mock<IStorageProvider>();
            moq.Setup(m => m.GetRecords(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<Unit>(),
                                        It.IsAny<IList<SeriesValue>>(), It.IsAny<bool>()))
                            .Returns(true);
            counter.StorageProvider = moq.Object;

            // act
            counter.GetSeries(Unit.kWh, DateTime.Today, DateTime.Today.AddDays(1), SeriesResolution.FiveMinutes, false);

            // assert
            moq.Verify(m => m.GetRecords(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<Unit>(),
                                            It.IsAny<IList<SeriesValue>>(), It.IsAny<bool>()), Times.AtLeastOnce());
        }

        [Test]
        public void WhenCounterGetSeriesIsCalledTheCounterPeakDetectorIsUsed()
        {
            // Arrange
            var peakDetectorMock = new Mock<ICounterPeakDetector>();
            peakDetectorMock.SetupSequence(m => m.IsPeak(It.IsAny<DateTime>(), It.IsAny<ulong>(), It.IsAny<decimal>(), It.IsAny<Record>(), It.IsAny<Counter>()))
                            .Returns(false)
                            .Returns(true);
            Counter.CounterPeakDetector = peakDetectorMock.Object;

            var mockStorageProvider = new Mock<IStorageProvider>();
            mockStorageProvider.Setup(m => m.SetValue(It.IsAny<DateTime>(), It.IsAny<ulong>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<Record>()))
                               .Returns(new Record(42, 42, 42, 0));
            mockStorageProvider.Setup(m => m.GetValue(It.IsAny<DateTime>()))
                               .Returns(new Record(42, 42, 42, 0));
            mockStorageProvider.Setup(m => m.FindPrevious(It.IsAny<DateTime>()))
                               .Returns(new Record(42, 42, 42, 0));

            Counter target = CreateCounterForSetValue(mockStorageProvider.Object);
            var qboxStatus = new QboxStatus();

            // Act
            target.SetValue(new DateTime(2019, 1, 1, 0, 0, 0), 42, qboxStatus);
            target.SetValue(new DateTime(2019, 1, 1, 0, 1, 0), 1000000000, qboxStatus);

            // Assert
            peakDetectorMock.Verify(m => m.IsPeak(It.IsAny<DateTime>(), It.IsAny<ulong>(), It.IsAny<decimal>(), It.IsAny<Record>(), It.IsAny<Counter>()),
                                    Times.Exactly(2));
            // The peak should just be ignored.
            mockStorageProvider.Verify(m => m.SetValue(new DateTime(2019, 1, 1, 0, 1, 0), It.IsAny<ulong>(), It.IsAny<decimal>(), It.IsAny<decimal>(), It.IsAny<Record>()), Times.Never);
        }

        private static Counter CreateCounterForSetValue(IStorageProvider storageProvider)
        {
            var counter = new Counter
            {
                QboxSerial = "00-00-000-000",
                CounterId = 181,
                StorageProvider = storageProvider,
                CounterSensorMappings = new List<CounterSensorMapping>
                {
                    new CounterSensorMapping
                    {
                        PeriodeBegin = new DateTime(2012, 1, 1),
                        Formule = 1000
                    }
                }
            };
            var measurementTime = new DateTime(2019, 1, 1);
            var qbox = new Mini
            {
                SerialNumber = "serial"
            };
            qbox.Counters.Add(counter);

            return counter;
        }

        private static void CreateStorageMock(Mini mini)
        {
            foreach (var counter in mini.Counters)
            {
                var mock = new MockStorageProvider(mini.SerialNumber, counter.CounterId, Precision.Wh);
                counter.StorageProvider = mock;
            }
        }
    }
}
